@extends('layouts.new')

@section('content')
<div class="mt-6 max-w-4xl mx-auto px-4 sm:px-6 lg:px-8 shadow sm:rounded-md bg-white">
  <!-- We've used 3xl here, but feel free to try other max-widths based on your needs -->
  <div class="max-w-4xl mx-auto">
    <div class="bg-white py-5 border-b border-gray-200">
        <h3 class="text-lg leading-6 font-medium text-gray-900">
            Vérifiez votre adresse Email
        </h3>
    </div>
    <div class="py-5">
        @if (session('resent'))
            <div class="alert alert-success" role="alert">
                {{ __('Un email vient d\'être envoyé à votre adresse email.') }}
            </div>
        @endif
        @if(Session::has('error'))
        <p class=" pb-3 alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('error') }}</p>
        @endif

        Avant de continuer, veuillez vérifier votre email car un lien de vérification vous a été envoyé.<br/>
        Si vous n'avez pas reçu l'email

            <form class="d-inline" method="POST" action="App\Http\Controllers\Auth\ResetController@verify">
                @csrf
                <button type="submit" class="text-blue" style="text-decoration:none;">{{ __('cliquez ici pour en recevoir un de nouveau') }}</button>.
            </form>
    </div>
  </div>
</div>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Verify Your Email Address</div>

                <div class="card-body">
                  @if (session('resent'))
                      <div class="alert alert-success" role="alert">
                          {{ __('A fresh verification link has been sent to your email address.') }}
                      </div>
                  @endif
                  @if(Session::has('error'))
                    <p class=" pb-3 alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('error') }}</p>
                  @endif

                    Before proceeding, please check your email for a verification link.
                    If you did not receive the email

                        <form class="d-inline" method="POST" action="App\Http\Controllers\Auth\ResetController@verify">
                            @csrf
                            <button type="submit" class="btn btn-link p-0 m-0 align-baseline" style="text-decoration:none;">{{ __('click here to request another') }}</button>.
                        </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
