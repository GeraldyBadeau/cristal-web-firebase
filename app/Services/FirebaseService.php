<?php

namespace App\Services;

use Kreait\Firebase\Firestore;

class FirebaseService
{
    public $database;
    public $serverTimestamp;

    public function __construct (Firestore $firestore)
    {
        $this->firestore = $firestore;
        $database = $firestore->database();
        $this->database = $database;
        //$this->serverTimestamp = $firestore.FieldValue.serverTimestamp();
    }

    public function firestoreGetAll($collection)
    {
        $collectionRef = $this->database->collection($collection);
        $documents = $collectionRef->documents();

        return $documents;
    }
}