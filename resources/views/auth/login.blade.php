@extends('layouts.new')

@section('content')
<div class="min-h-screen bg-white flex">
  <div class="flex-1 flex flex-col justify-center py-12 px-4 sm:px-6 lg:flex-none lg:px-20 xl:px-24">
    <div class="mx-auto w-full max-w-sm lg:w-96">
      <div>
        <img class="h-12 w-auto" src="https://tailwindui.com/img/logos/workflow-mark-indigo-600.svg" alt="Workflow">
        <h2 class="mt-6 text-3xl font-extrabold text-gray-900">
          Connectez-vous
        </h2>
        <p class="mt-2 text-sm text-gray-600">
          Ou
          <a href="{{ route('register') }}" class="font-medium text-indigo-600 hover:text-indigo-500">
            créez un compte
          </a>
        </p>
      </div>

      <div class="mt-8">

        <div class="mt-6">
          <form method="POST" action="{{ route('login') }}" class="space-y-6">
            @csrf
            <div>
              <label for="email" class="block text-sm font-medium text-gray-700">
                Email
              </label>
              <div class="mt-1">
                <input id="email" name="email" type="email" autocomplete="email" value="{{ old('email') }}" required class="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
              </div>
            </div>

            <div class="space-y-1">
              <label for="password" class="block text-sm font-medium text-gray-700">
                Mot de passe
              </label>
              <div class="mt-1">
                <input id="password" name="password" type="password" autocomplete="current-password" required class="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
              </div>
            </div>

            <div class="flex items-center justify-between">
              <div class="flex items-center">
                <input id="remember" name="remember" type="checkbox" {{ old('remember') ? 'checked' : '' }} class="h-4 w-4 text-indigo-600 focus:ring-indigo-500 border-gray-300 rounded">
                <label for="remember" class="ml-2 block text-sm text-gray-900">
                  Se rappeler de moi
                </label>
              </div>

              <div class="text-sm">
                <a href="/password/reset" class="font-medium text-indigo-600 hover:text-indigo-500">
                  Mot de passe oublié?
                </a>
              </div>
            </div>

            <div>
              <button type="submit" class="w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                Connexion
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="hidden lg:block relative w-0 flex-1">
    <img class="absolute inset-0 h-full w-full object-cover" src="img/driver_close_low.jpg" alt="">
  </div>
</div>

<script src="https://www.gstatic.com/firebasejs/7.14.0/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.14.0/firebase-auth.js"></script>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script>
// Initialize Firebase
var firebaseConfig = {
  apiKey: "AIzaSyD6SOeG3sL7etC0ZucH6NMQqpMF8Mkxc6Q",
  authDomain: "cristal-chauffeurs-bfefa.firebaseapp.com",
  projectId: "cristal-chauffeurs-bfefa",
  storageBucket: "cristal-chauffeurs-bfefa.appspot.com",
  messagingSenderId: "500110386820",
  appId: "1:500110386820:web:9fc08d94114868244dc4d1"
};
firebase.initializeApp(config);
var facebookProvider = new firebase.auth.FacebookAuthProvider();
var googleProvider = new firebase.auth.GoogleAuthProvider();
var facebookCallbackLink = '/login/facebook/callback';
var googleCallbackLink = '/login/google/callback';
async function socialSignin(provider) {
  var socialProvider = null;
  if (provider == "facebook") {
    socialProvider = facebookProvider;
    document.getElementById('social-login-form').action = facebookCallbackLink;
  } else if (provider == "google") {
    socialProvider = googleProvider;
    document.getElementById('social-login-form').action = googleCallbackLink;
  } else {
    return;
  }
  firebase.auth().signInWithPopup(socialProvider).then(function(result) {
    result.user.getIdToken().then(function(result) {
      document.getElementById('social-login-tokenId').value = result;
      document.getElementById('social-login-form').submit();
    });
  }).catch(function(error) {
    // do error handling
    console.log(error);
  });
}
</script>



  

@endsection
