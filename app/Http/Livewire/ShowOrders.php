<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Services\FirebaseService;
use PDF;

class ShowOrders extends Component
{

    /**
     * Put your custom public properties here!
     */
    private $firebaseService;
    private $modelId;
    private $pdf;

    public $availableDrivers;

    public function mount(FirebaseService $firebaseService) 
    {
        $this->firebaseService = $firebaseService;
        $usersRef = $this->firebaseService->database->collection('users');
        $query = $usersRef->where("isActive","=","true")->where("isOnline","=","true");
        $users = $query->documents();
    }

    /**
     * Loads the model data
     * of this component.
     *
     * @return void
     */
    public function loadModel(FirebaseService $firebaseService)
    {
        $orderRef = $firebaseService->database->collection('taxi_trips')->document($this->modelId);
        $snapshot = $orderRef->snapshot();

        return $snapshot;
    }

    public function read(FirebaseService $firebaseService)
    {
        $ordersRef = $firebaseService->database->collection('taxi_trips');
        $query = $ordersRef->where("status","=","trip_completed")->orderBy("tripEndTime", "desc");
        $documents = $query->documents();

        return $documents->rows();
    }

    public function generatePDF($id,FirebaseService $firebaseService)
    {
        //$pdf = App::make('dompdf.wrapper');
        $this->modelId = $id;
        $snapshot = $this->loadModel($firebaseService);
        
        // $orderRef = $this->firebaseService->database->collection('taxi_trips')->document($id);
        // $snapshot = $orderRef->snapshot();

        $this->pdf = PDF::loadView('invoice');;
        //dd($pdf);
        return $this->pdf->stream('invoice.pdf');
    
    }
    
    public function render(FirebaseService $firebaseService)
    {
        return view('livewire.show-orders', [
            'data' => $this->read($firebaseService),
        ]);
    }
}
