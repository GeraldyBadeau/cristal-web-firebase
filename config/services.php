<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'firebase' => [
        'apiKey' => "AIzaSyD6SOeG3sL7etC0ZucH6NMQqpMF8Mkxc6Q",
        'authDomain' => "cristal-chauffeurs-bfefa.firebaseapp.com",
        'projectId' => "cristal-chauffeurs-bfefa",
        'storageBucket' => "cristal-chauffeurs-bfefa.appspot.com",
        'messagingSenderId' => "500110386820",
        'appId' => "1:500110386820:web:9fc08d94114868244dc4d1",
    ],
];
