<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Kreait\Firebase\Auth as FirebaseAuth;
use Kreait\Firebase\Exception\FirebaseException;
use Illuminate\Validation\ValidationException;
use Session;
use App\Services\FirebaseService;
use Google\Cloud\Firestore\FieldValue;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;
    protected $auth;
    private $firebaseService;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
     protected $redirectTo = RouteServiceProvider::HOME;
    public function __construct(FirebaseAuth $auth, FirebaseService $firebaseService) {
       $this->middleware('guest');
       $this->auth = $auth;
       $this->firebaseService = $firebaseService;
    }
    protected function validator(array $data) {
       return Validator::make($data, [
          'name' => ['required', 'string', 'max:255'],
          'email' => ['required', 'string', 'email', 'max:255'],
          'password' => ['required', 'string', 'min:8', 'max:12', 'confirmed'],
       ]);
    }
    protected function register(Request $request) {
       try {
         $this->validator($request->all())->validate();
         $userProperties = [
            'email' => $request->input('email'),
            'emailVerified' => false,
            'password' => $request->input('password'),
            'displayName' => $request->input('name'),
            'disabled' => false,
         ];
         $createdUser = $this->auth->createUser($userProperties);
         $usersRef = $this->firebaseService->database->collection('users')->document($createdUser->uid);
         
         if ($request->input('role') == 'driver')
         {
            $usersRef->set([
               'id'=> $createdUser->uid,
               'userID'=> $createdUser->uid, 
               'email' => $request->input('email'),
               'firstName'=> $request->input('firstName'),
               'lastName'=> $request->input('name'),
               'username'=> $request->input('email'),
               'phone'=> '',
               'inProgressOrderID' => null,
               'role' => 'driver',
               'isApproved' => false,
               'createdAt'=> FieldValue::serverTimestamp(),
           ], ['merge' => true]);
         }else{
            $usersRef->set([
               'id'=> $createdUser->uid,
               'userID'=> $createdUser->uid, 
               'email' => $request->input('email'),
               'firstName'=> $request->input('firstName'),
               'lastName'=> $request->input('name'),
               'username'=> $request->input('email'),
               'phone'=> '',
               'isApproved' => false,
               'createdAt'=> FieldValue::serverTimestamp(),
           ], ['merge' => true]);
         }
         $usersRef->set([
            'id'=> $createdUser->uid,
            'userID'=> $createdUser->uid, 
            'email' => $request->input('email'),
            'firstName'=> $request->input('firstName'),
            'lastName'=> $request->input('name'),
            'username'=> $request->input('email'),
            'phone'=> '',
            'createdAt'=> FieldValue::serverTimestamp(),
        ], ['merge' => true]);
         return redirect()->route('login');
       } catch (FirebaseException $e) {
          Session::flash('error', $e->getMessage());
          return back()->withInput();
       }
    }
 }
