<?php

namespace App\Http\Livewire;

use Mapper;
use App\Models\User;
use Livewire\Component;

class DriverMap extends Component
{
    public $drivers;

    public function mount()
    {
        Mapper::map(48.860014609088154, 2.3387470903320473, ['overlay' => 'TRAFFIC']);
        $this->drivers = User::where('role_id','=',3)->with('position')->get();

        foreach ($this->drivers as $driver) {
            if (isset($driver->position->longitude) && isset($driver->position->latitude)) {
                Mapper::informationWindow($driver->position->latitude, $driver->position->longitude, $driver->first_name." ".$driver->name, ['open' => true, 'maxWidth'=> 300, 'autoClose' => true, 'marker' => false, 'markers' => ['animation' => 'DROP', 'title' => 'title']]);
            }
        }
    }

    public function render()
    {
        return view('livewire.driver-map');
    }
}
