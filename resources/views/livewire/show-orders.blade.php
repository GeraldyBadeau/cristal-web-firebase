<div>
    
    <div class="col-4">
    <label for="location" class="text-sm font-medium text-gray-700">Mois</label>
    <select id="location" name="location" class="mt-1 ml-2 pl-3 pr-10 py-2 text-base border-gray-300 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm rounded-md">
        <option>Janvier</option>
        <option selected>Février</option>
        <option>Mars</option>
    </select>
    </div>


    <!-- This example requires Tailwind CSS v2.0+ -->
    <div class="mt-4 flex flex-col">
    <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
        <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
            <table class="min-w-full divide-y divide-gray-200">
            <thead class="bg-gray-50">
                <tr>
                <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                    Nom
                </th>
                <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                    Trajet
                </th>
                <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                    Statut
                </th>
                <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                    Montant
                </th>
                <th scope="col" class="relative px-6 py-3">
                    <span class="sr-only">Edit</span>
                </th>
                </tr>
            </thead>
            <tbody class="bg-white divide-y divide-gray-200">
                @foreach ($data as $trip)
                <tr>
                <td class="px-6 py-4 whitespace-nowrap">
                    @isset($trip->data()['passenger']['firstName'])
                        <div class="text-sm font-medium text-gray-900">
                        {{$trip->data()['passenger']['firstName']}} {{$trip->data()['passenger']['lastName']}}
                        </div>
                        <div class="text-sm text-gray-500">
                        {{$trip->data()['passenger']['email']}}
                        </div>
                    @endisset
                </td>
                <td class="px-6 py-4 whitespace-nowrap">
                    <div class="text-xs text-gray-500">{{ Str::limit($trip->data()['pickup']['title'].' '.$trip->data()['pickup']['subtitle'],40) }} </div>
                    <div class="text-xs text-gray-500">{{ Str::limit($trip->data()['dropoff']['title'].' '.$trip->data()['dropoff']['subtitle'],40) }} </div>
                </td>
                <td class="px-6 py-4 whitespace-nowrap">
                    <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">
                    Terminée
                    </span>
                    <div class="text-sm text-gray-500">{{ \Carbon\Carbon::createFromTimestamp( $trip->data()['tripEndTime']/1000 )->format('d/m/Y H:i:s') }}</div>
                </td>
                <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900">
                {{$trip->data()['price']}} €
                </td>
                <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                    <a href="{{ route('invoice',[ 'id' => $trip->data()['id'] ]) }}" target="_blank" class="text-indigo-600 hover:text-indigo-900">PDF</a>
                </td>
                </tr>
                @endforeach
                <!-- More people... -->
            </tbody>
            </table>
        </div>
        </div>
    </div>
    </div>

</div>
