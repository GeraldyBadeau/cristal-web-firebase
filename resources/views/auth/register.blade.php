@extends('layouts.app')

<div class="min-h-screen bg-white flex">
  <div class="flex-1 flex flex-col justify-center py-12 px-4 sm:px-6 lg:flex-none lg:px-20 xl:px-24">
    <div class="mx-auto w-full max-w-sm lg:w-96">
      <div>
        <img class="h-12 w-auto" src="https://tailwindui.com/img/logos/workflow-mark-indigo-600.svg" alt="Workflow">
        <h2 class="mt-6 text-3xl font-extrabold text-gray-900">
          Enregistrez-vous
        </h2>
      </div>

      <div class="mt-8">

        <div class="mt-6">
          <form method="POST" class="space-y-6" action="{{ route('register') }}">
          @csrf
            <div>
              <label for="firstName" class="block text-sm font-medium text-gray-700">
                Prénom
              </label>
              <div class="mt-1">
                <input id="firstName" name="firstName" type="text" autocomplete="firstName" required class="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
              </div>
              @error('firstName')
                <span class="mt-2 text-sm text-red-600" role="alert">
                  {{ $message }} test
                </span>
              @enderror
            </div>

            <div>
              <label for="name" class="block text-sm font-medium text-gray-700">
                Nom
              </label>
              <div class="mt-1">
                <input id="name" name="name" type="text" autocomplete="name" required class="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
              </div>
              @error('name')
                <span class="mt-2 text-sm text-red-600" role="alert">
                  {{ $message }} test
                </span>
              @enderror
            </div>

            <div class="space-y-1">
              <label for="email" class="block text-sm font-medium text-gray-700">
                Email address
              </label>
              <div class="mt-1">
                <input id="email" name="email" type="email" autocomplete="email" required class="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
              </div>
            </div>

            <div class="space-y-1">
              <label for="password" class="block text-sm font-medium text-gray-700">
                Mot de passe
              </label>
              <div class="mt-1">
                <input id="password" name="password" type="password" autocomplete="new-password" required class="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
              </div>
            </div>

            <div class="space-y-1">
              <label for="password-confirm" class="block text-sm font-medium text-gray-700">
                Confirmation
              </label>
              <div class="mt-1">
                <input id="password-confirm" name="password_confirmation" type="password" autocomplete="new-password" required class="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
              </div>
            </div>

            <div>
              <label for="role" class="block text-sm font-medium text-gray-700">Vous êtes</label>
              <select id="role" name="role" class="mt-1 block w-full pl-3 pr-10 py-2 text-base border-gray-300 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm rounded-md">
                <option value="client">Client</option>
                <option value="driver" selected>Chauffeur</option>
                <option value="manager">Gestionnaire</option>
              </select>
            </div>



            <div>
              <button type="submit" class="w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                S'enregistrer
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="hidden lg:block relative w-0 flex-1">
    <img class="absolute inset-0 h-full w-full object-cover" src="img/driver_low.jpg" alt="">
    Photo by <a href="https://unsplash.com/@chethankvs?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Chethan KVS</a> on <a href="https://unsplash.com/s/photos/driver?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>
  
  </div>
</div>


