<h1>Facture</h1><br/>
Identifiant course: {{$id ?? ''}}<br/>
<h2 class="my-4">Course</h2>
    <div class="container">
        {{ \Carbon\Carbon::createFromTimestamp( $snapshot->data()['tripEndTime']/1000 )->format('d/m/Y H:i:s') }}<br/>
        <span>De: </span><span>{{ $snapshot->data()['pickup']['title'].' '.$snapshot->data()['pickup']['subtitle'] }}</span><br/>
        <span>A: </span><span>{{ $snapshot->data()['dropoff']['title'].' '.$snapshot->data()['dropoff']['subtitle'] }}</span><br/>
        <span>Durée: </span><span></span><br/>
        <span>Montant: </span><span>{{ $snapshot->data()['price'] }} €</span><br/>
    </div>

@isset($snapshot->data()['passenger']['firstName'])
<h2 class="my-4">Passager</h2>
    <div class="container">
        <span>{{ $snapshot->data()['passenger']['firstName']}} {{$snapshot->data()['passenger']['lastName'] }}</span><br/>
        <span>{{ $snapshot->data()['passenger']['email'] }}</span><br/>
    </div>
@endisset

<h2 class="my-4">Chauffeur</h2>
    <div class="container">
        <span>{{ $snapshot->data()['driver']['firstName']}} {{$snapshot->data()['driver']['lastName'] }}</span><br/>
        <span>Voiture: </span><span>{{ $snapshot->data()['driver']['carName'] }} {{ $snapshot->data()['driver']['carNumber'] }}</span><br/>
    </div>


