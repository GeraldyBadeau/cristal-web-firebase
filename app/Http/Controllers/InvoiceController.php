<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\FirebaseService;
use PDF;

class InvoiceController extends Controller
{
    private $firebaseService;

    public function __construct(FirebaseService $firebaseService)
    {
        $this->middleware('auth');
        $this->firebaseService = $firebaseService;
    }

    public function invoice($id) 
    {
        $orderRef = $this->firebaseService->database->collection('taxi_trips')->document($id);
        $snapshot = $orderRef->snapshot();
        
        $pdf = PDF::loadView('invoice',['id' => $id, 'snapshot' => $snapshot]);
        return $pdf->stream('invoice.pdf');
    }
}
