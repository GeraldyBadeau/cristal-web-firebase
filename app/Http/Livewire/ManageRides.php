<?php

namespace App\Http\Livewire;

use App\Models\Ride;
use App\Models\User;
use Livewire\Component;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ManageRides extends Component
{
    public $showEditDriverModal = false;

    public $modelId;
    public $drivers;
    public $driver = 0;

    /**
     * The validation rules
     *
     * @return void
     */
    public function rules()
    {
        return [
            'driver' => 'required',
            //'slug' => ['required', Rule::unique('pages', 'slug')->ignore($this->modelId)],
        ];
    }

    public function mount()
    {
        $this->drivers = User::where('role_id','=',3)->get();
    }

    /**
     * The create function.
     *
     * @return void
     */
    public function create()
    {
        $this->validate();
        Ride::create($this->modelData());
        $this->showEditDriverModal = false;
        $this->reset();

        $this->dispatchBrowserEvent('event-notification', [
            'eventName' => 'New Ride',
            'eventMessage' => 'Another ride has been created!',
        ]);
    }

    /**
     * The read function.
     *
     * @return void
     */
    public function read()
    {
        if(Auth::user()->role_id == 1 || Auth::user()->role_id == 2)
        {
            return Ride::with('driver','depart','arrivee','invoice_entity','order_entity','dysfunction','agents','status')
            
            ->orderBy('dateRealisation', 'asc')
            ->orderBy('heureDepart', 'asc')
            ->get();
        }else{
            return Ride::currentDriver()
            ->with('driver','depart','arrivee','invoice_entity','order_entity','dysfunction','agents','status')
            
            ->orderBy('dateRealisation', 'asc')
            ->orderBy('heureDepart', 'asc')
            ->get();
        }
    }

    /**
     * The update function.
     *
     * @return void
     */
    public function update()
    {
        $this->validate();
        Ride::find($this->modelId)->update($this->modelData());
        $this->showEditDriverModal = false;
        $this->updatedDriver(0);

        $this->dispatchBrowserEvent('event-notification', [
            'eventName' => 'Updated Ride',
            'eventMessage' => 'There is a ride (' . $this->modelId . ') that has been updated!',
        ]);
    }

    /**
     * The delete function.
     *
     * @return void
     */
    public function delete()
    {
        Ride::destroy($this->modelId);
        $this->modalConfirmDeleteVisible = false;
        $this->resetPage();

        $this->dispatchBrowserEvent('event-notification', [
            'eventName' => 'Deleted Ride',
            'eventMessage' => 'The ride (' . $this->modelId . ') has been deleted!',
        ]);
    }

    /**
     * Runs everytime the title
     * variable is updated.
     *
     * @param  mixed $value
     * @return void
     */
    public function updatedDriver($value)
    {
        $this->driver = $value;
    }

    /**
     * Shows the form modal
     * of the create function.
     *
     * @return void
     */
    public function createShowModal()
    {
        $this->resetValidation();
        $this->reset();
        $this->showEditDriverModal = true;
    }

    /**
     * Shows the form modal
     * in update mode.
     *
     * @param  mixed $id
     * @return void
     */
    public function updateShowModal($id)
    {
        //$this->resetValidation();
        //$this->reset();
        $this->modelId = $id;
        $this->showEditDriverModal = true;
        $this->loadModel();
    }

    /**
     * Update ride status id
     *
     * @param  mixed $id
     * @param  mixed $status_id
     * @return void
     */
    public function updateRide($id, $status_id)
    {
        $this->modelId = $id;
        $this->validate();
        Ride::find($this->modelId)->update(['status_id' => $status_id]);

        $this->dispatchBrowserEvent('event-notification', [
            'eventName' => 'Updated Ride',
            'eventMessage' => 'There is a ride (' . $this->modelId . ') that has been updated!',
        ]);
    }

    /**
     * Shows the delete confirmation modal.
     *
     * @param  mixed $id
     * @return void
     */
    public function deleteShowModal($id)
    {
        $this->modelId = $id;
        $this->modalConfirmDeleteVisible = true;
    }

    /**
     * Loads the model data
     * of this component.
     *
     * @return void
     */
    public function loadModel()
    {
        $data = Ride::find($this->modelId);

        $sqlDistance = DB::raw('( 111.045 * acos( cos( radians(' . $data->depart->latitude . ') ) 
       * cos( radians( last_positions.latitude ) ) 
       * cos( radians( last_positions.longitude ) 
       - radians(' . $data->depart->longitude . ') ) 
       + sin( radians(' . $data->depart->latitude  . ') ) 
       * sin( radians( last_positions.latitude ) ) ) )');

        $this->drivers = User::join('last_positions','users.id','=','last_positions.user_id')->where('role_id','=',3)->select('users.*','last_positions.longitude','last_positions.latitude')->selectRaw("{$sqlDistance} AS distance")->orderBy('distance')->get();
        if (isset($data->driver))
        {
            $this->driver = $data->driver->id;
        }
        
    }

    /**
     * The data for the model mapped
     * in this component.
     *
     * @return void
     */
    public function modelData()
    {
        return [
            'chauffeur' => $this->driver,
            'status_id' => 2
        ];
    }

    /**
     * Dispatch event
     *
     * @return void
     */
    public function dispatchEvent()
    {
        $this->dispatchBrowserEvent('event-notification', [
            'eventName' => 'Sample Event',
            'eventMessage' => 'You have a sample event notification!',
        ]);
    }
    
    public function render()
    {
        return view('livewire.manage-rides', [
            'data' => $this->read(),
        ]);
    }
}
