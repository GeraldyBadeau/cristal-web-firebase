@extends('layouts.new')

@section('content')
<div class="container mx-auto">
    <div class="row justify-content-center">
        <div class="container col-md-12 mx-auto">
            {{-- <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
            </div> --}}
            <div class="container bg-white shadow overflow-hidden sm:rounded-md mt-6 mx-auto">

                <div class="px-4 py-4 sm:px-6">
                    @livewire('show-orders')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
